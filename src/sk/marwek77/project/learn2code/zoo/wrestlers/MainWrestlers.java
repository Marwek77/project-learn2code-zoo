package sk.marwek77.project.learn2code.zoo.wrestlers;


public class MainWrestlers {

    public static void main(String[] args) {

        Kane wrestler1 = new Kane();
        StoneCold wrestler2 = new StoneCold();

        wrestler1.themeMusic();
        wrestler1.finisher();
        wrestler1.paymentForWork(5);

        wrestler2.themeMusic();
        wrestler2.finisher();
        wrestler2.paymentForWork(5);

    }
}
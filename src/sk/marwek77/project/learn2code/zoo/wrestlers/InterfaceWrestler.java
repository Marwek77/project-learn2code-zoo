package sk.marwek77.project.learn2code.zoo.wrestlers;

public interface InterfaceWrestler {

    void themeMusic();
    void finisher();
    void paymentForWork(int hours);

}
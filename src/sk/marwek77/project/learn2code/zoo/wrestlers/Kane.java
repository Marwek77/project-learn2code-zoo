package sk.marwek77.project.learn2code.zoo.wrestlers;

public class Kane implements InterfaceWrestler {

    @Override
    public void themeMusic() {
        System.out.println("Kane's Intro Music");
    }

    @Override
    public void finisher() {
        System.out.println("Tombstone");
    }

    @Override
    public void paymentForWork(int hours) {
        System.out.println("Kane will make USD" + hours*200 + " for this match");
    }
}
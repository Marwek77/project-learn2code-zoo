package sk.marwek77.project.learn2code.zoo.wrestlers;

public class StoneCold implements InterfaceWrestler {

    @Override
    public void themeMusic() {
        System.out.println("Stone Cold's Intro Music");
    }

    @Override
    public void finisher() {
        System.out.println("Stunner");
    }

    @Override
    public void paymentForWork(int hours) {
        System.out.println("Stone Cold will make USD" + hours*300 + " for this match");
    }
}
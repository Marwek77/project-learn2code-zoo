package sk.marwek77.project.learn2code.zoo.test;

class Food {

    private String foodA;

    Food(String A) {

        foodA = A;
    }

    String getFood() {

        return foodA; //use return when value is returned
    }

    void systemPrint() { // void only if nothing is returned

        System.out.println(foodA);
    }
}

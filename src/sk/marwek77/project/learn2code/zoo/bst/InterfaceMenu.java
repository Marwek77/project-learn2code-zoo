package sk.marwek77.project.learn2code.zoo.bst;

interface InterfaceMenu {

  //  #M
  void menuMain3D();

  //  #MA1
  void menuList3D();

  //  #M4
  void menuSubSort3D();

  //  #M4A1
  void menuSubList3D();

  //  #M+ listing method
  void menuAnimalsAll3D();

  //  #M auxiliary method
  void delayIt(int i);

}
package sk.marwek77.project.learn2code.zoo.bst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class Test2 {

  public static void main(String[] args) {
    ArrayList<String[]> listOfStringArrays = new ArrayList<>();
    listOfStringArrays.add(new String[] {"x","c","10"});
    listOfStringArrays.add(new String[] {"a","t","2"});
    listOfStringArrays.add(new String[] {"m","n","7"});
    listOfStringArrays.sort(Comparator.comparing(strings -> strings[0]));
    for (int i = 0; i < listOfStringArrays.size(); i++) {
      String[] sa = listOfStringArrays.get(i);
      System.out.println(Arrays.toString(sa));
    }
        /* prints out
          [a, b, c]
          [m, n, o]
          [x, y, z]
        */

  }

}
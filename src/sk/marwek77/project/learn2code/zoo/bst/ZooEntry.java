package sk.marwek77.project.learn2code.zoo.bst;

class ZooEntry extends ZooInputs {

  int inputReturnInt() {

    /* All changes if necessary here, not in universal number reading method */

    return readInteger();
  }

  String inputReturnString() {

    /* All changes if necessary here, not in universal string reading method */

    return readString();
  }

}
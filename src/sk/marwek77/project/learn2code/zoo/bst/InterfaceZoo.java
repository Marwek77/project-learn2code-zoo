package sk.marwek77.project.learn2code.zoo.bst;

interface InterfaceZoo {

//  #1
    void animalAddIt3D();

//  #2
    void animalRemoveIt3D();

//  #3
    void animalListIt3D();

//  #4.1 & #4.2 & #4.3
    void animalSortIt3DPos0();
    void animalSortIt3DPos1();
    void animalSortIt3DPos2();

//  #5 auxiliary method
    int checkIt3D();

}
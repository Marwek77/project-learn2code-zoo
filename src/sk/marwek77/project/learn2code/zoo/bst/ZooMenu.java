package sk.marwek77.project.learn2code.zoo.bst;

import java.util.concurrent.TimeUnit;

class ZooMenu {

  /* This class is for operations with Menu only */

  private final ZooEntry zooEntry = new ZooEntry();
  private final ZooMethodsDogs zooMethodsDogs = new ZooMethodsDogs();
  private final ZooMethodsCats zooMethodsCats = new ZooMethodsCats();
  private final ZooMethodsHamsters zooMethodsHamsters = new ZooMethodsHamsters();

  /* ------------------- */

  void menuStart() {

    /* Method for choosing from 3D Menu list */

    int status, exit = 5; /* value to end the program */
    do {
      menuListStart();
      status = zooEntry.inputReturnInt();
      switch (status) {
        case 1:
          zooMethodsDogs.menuMain3D(); /* OK now */
          break;
        case 2:
          zooMethodsCats.menuMain3D(); /* OK now */
          break;
        case 3:
          zooMethodsHamsters.menuMain3D(); /* OK now */
          break;
        case 4:
          zooMethodsDogs.menuAnimalsAll3D(); /* OK now */
          zooMethodsCats.menuAnimalsAll3D(); /* OK now */
          zooMethodsHamsters.menuAnimalsAll3D(); /* OK now */
          break;
        case 5:
          System.out.println("Koniec konfigurátora, dovidenia!");
          delayIt(1);
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }

  private void menuListStart() {

    /* Method for printing Menu List Start  */

    System.out.println();
    System.out.println("         Zoo konfigurátor 3D:");
    System.out.println();
    System.out.println("Voľba 1: Práca so psami");
    System.out.println("Voľba 2: Práca s mačkami");
    System.out.println("Voľba 3: Práca so škrečkami");
    System.out.println("Voľba 4: Zoznam všetkých zvierat");
    System.out.println("Voľba 5: Koniec konfigurátora");
    System.out.println();
  }

  /* ------------------- */

  private void delayIt(int i) {

    /* Method for delaying messages */

    try {
      TimeUnit.SECONDS.sleep(i);
    } catch (InterruptedException e) {
      e.printStackTrace(); /* Not sure what else can be here */
    }
  }

}
package sk.marwek77.project.learn2code.zoo.bst;

import java.util.Scanner;
import java.util.InputMismatchException;

public class Test {

  public static void main(String[] args) {

    Scanner scan = new Scanner(System.in);
    int number = 0;

    boolean weHaveException = true;

    do {
      try {
        System.out.print("" + "(zadaj svoju voľbu): ");
        number = scan.nextInt();
        weHaveException = false;
      }
      catch (InputMismatchException e) {
        System.out.println("Zadaný reťazec nie je celé číslo.");
        System.out.println("Opakujte zadanie!");
        scan.nextLine();
      }
    } while (weHaveException);

  }
}
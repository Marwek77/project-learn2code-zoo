package sk.marwek77.project.learn2code.zoo.bst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test3 {

  public static void main(String[] args) {

    animalAddIt3DTest();

//    System.out.println(rowList.get(1)[1]); // prints "e"

  }

    private static void animalAddIt3DTest() {

      /* Method for adding 3 dimensional animals into the list */

      String a, b;
      int c;
      ZooEntry zooEntry = new ZooEntry();
      List<String[]> rowList = new ArrayList<>();

      System.out.print("Zadaj meno zvieraťa:");
      a = zooEntry.inputReturnString();
      System.out.print("Zadaj meno zvieraťa:");
      b = zooEntry.inputReturnString();
      System.out.print("Zadaj meno zvieraťa:");
      c = zooEntry.inputReturnInt();
      rowList.add(new String[]{String.valueOf(a), String.valueOf(b), String.valueOf(c)});
      System.out.println();

      System.out.print("Zadaj meno zvieraťa:");
      a = zooEntry.inputReturnString();
      System.out.print("Zadaj meno zvieraťa:");
      b = zooEntry.inputReturnString();
      System.out.print("Zadaj meno zvieraťa:");
      c = zooEntry.inputReturnInt();
      rowList.add(new String[]{String.valueOf(a), String.valueOf(b), String.valueOf(c)});
      System.out.println();

      for (String[] strings : rowList) {
        System.out.print(Arrays.toString(strings));
      }

      System.out.println();

      for (String[] strings : rowList) {
        for (String string : strings) System.out.print(string + " ");
        System.out.println();
      }
    }
}
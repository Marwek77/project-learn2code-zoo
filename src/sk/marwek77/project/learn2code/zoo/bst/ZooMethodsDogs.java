package sk.marwek77.project.learn2code.zoo.bst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

class ZooMethodsDogs implements InterfaceZoo, InterfaceMenu {

  /* This class is for operations with Dogs only */

  private final ZooEntry zooEntry = new ZooEntry();
  private final List<String[]> animals3D = new ArrayList<>();

  /* ----------------------------------------------- */
  /* InterfaceZoo implementation for 3D Dogs version */
  /* ----------------------------------------------- */

  @Override
  public void animalAddIt3D() {

    /* Method for adding animals into the 3D list */

    System.out.println();
    System.out.print("Zadaj meno nového psa:");
    String animalName = zooEntry.inputReturnString();
    System.out.print("Zadaj sfarbenie srsti nového psa:");
    String animalColor = zooEntry.inputReturnString();
    System.out.print("Zadaj vek nového psa:");
    int animalAge = zooEntry.inputReturnInt();
    animals3D.add(new String[]{String.valueOf(animalName), String.valueOf(animalColor), String.valueOf(animalAge)});
    System.out.println();
    System.out.print("Pridali sme psa: " + Arrays.toString(animals3D.get(animals3D.size()-1)));
    System.out.println();
  }

  @Override
  public void animalRemoveIt3D() {

    /* Method for removing animals from the 3D list */

    if (animals3D.size() != 0) {
      animalListIt3D();
      System.out.println();
      System.out.println("Vyber psa, ktorého odoberieme z aktuálneho zoznamu:");
      int i = checkIt3D();
      System.out.println("Odoberáme psa číslo " + i + ": " + Arrays.toString(animals3D.get(i - 1)));
      animals3D.remove(i - 1);
      animalListIt3D();
    } else System.out.println("Zoznam psov je prázdny, niet čo odoberať!"); /* Zero animals on the 3D list */
  }

  @Override
  public void animalListIt3D() {

    /* Method for listing of 3D animals */

    System.out.println();
    System.out.println("Aktuálny počet psov v Zoo [meno, sfarbenie, vek]: " + animals3D.size());

    for (int counter = 0; counter < animals3D.size(); counter++) {
      String[] strings = animals3D.get(counter);
      System.out.print(counter + 1 + ". " + Arrays.toString(strings) + "  ");
    }
    System.out.println();
  }

  @Override
  public void animalSortIt3DPos0() {

    /* Method for sorting animals in the 3D list by name */

    System.out.println();
    System.out.println("Aktuálny zoznam psov zoradený podľa mena:");
    animals3D.sort(Comparator.comparing(strings -> strings[0])); /* solution from https://stackoverflow.com/questions/4699807/sort-arraylist-of-array-in-java */
    animalListIt3D();
  }

  @Override
  public void animalSortIt3DPos1() {

    /* Method for sorting animals in the 3D list by color */

    System.out.println();
    System.out.println("Aktuálny zoznam psov zoradený podľa sfarbenia:");
    animals3D.sort(Comparator.comparing(strings -> strings[1])); /* solution from https://stackoverflow.com/questions/4699807/sort-arraylist-of-array-in-java */
    animalListIt3D();
  }

  @Override
  public void animalSortIt3DPos2() {

    /* Method for sorting animals in the 3D list by age */

    System.out.println();
    System.out.println("Aktuálny zoznam psov zoradený podľa veku:");
    animals3D.sort(Comparator.comparing(strings -> strings[2])); /* solution from https://stackoverflow.com/questions/4699807/sort-arraylist-of-array-in-java */
    animalListIt3D();
  }

  @Override
  public int checkIt3D() {

    /* Method for checking your choice when removing animals from the 3D list */

    int i;
    do {
      i = zooEntry.inputReturnInt();
    } while (i < 1 || i > animals3D.size());
    return i;
  }

  /* ------------------------------------------------ */
  /* InterfaceMenu implementation for 3D Dogs version */
  /* ------------------------------------------------ */

  @Override
  public void menuMain3D() {

    /* Method for choosing from 3D menu list */

    int status, exit = 5; /* value to end the program */
    do {
      menuList3D();
      status = zooEntry.inputReturnInt();
      switch (status) {
        case 1:
          animalAddIt3D(); /* OK now */
          break;
        case 2:
          animalRemoveIt3D(); /* OK now */
          break;
        case 3:
          animalListIt3D(); /* OK now */
          break;
        case 4:
          if (animals3D.size() != 0) {
            menuSubSort3D();
          } else System.out.println("Zoznam psov je prázdny, niet čo zoraďovať!"); /* Zero animals on the 3D list */
          break;
        case 5:
          System.out.println("Koniec konfigurátora, dovidenia!");
          delayIt(1);
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }

  @Override
  public void menuList3D() {

    /* Method for printing 3D menu list */

    System.out.println();
    System.out.println("         Konfigurátor psov:");
    System.out.println();
    System.out.println("Voľba 1: Pridanie psa");
    System.out.println("Voľba 2: Odobratie psa");
    System.out.println("Voľba 3: Zoznam psov");
    System.out.println("Voľba 4: Zoradenie psov podľa možností");
    System.out.println("Voľba 5: Koniec konfigurátora psov");
    System.out.println();
  }

  @Override
  public void menuSubSort3D() {

    /* Method for choosing from Submenu list for sorting */

    int status, exit = 4; /* value to end the program */
    do {
      menuSubList3D();
      status = zooEntry.inputReturnInt();
      switch (status) {
        case 1:
          animalSortIt3DPos0(); /* OK now */
          break;
        case 2:
          animalSortIt3DPos1(); /* OK now */
          break;
        case 3:
          animalSortIt3DPos2(); /* OK now */
          break;
        case 4:
          System.out.println("Koniec triedenia, návrat do menu!");
          delayIt(1);
          break;
        default:
          System.out.println("Nesprávna voľba!");
      }
    } while (status != exit);
  }

  @Override
  public void menuSubList3D() {

    /* Method for printing 3D Sublist */

    System.out.println();
    System.out.println("Voľba 1: Zoradenie psov podľa mena");
    System.out.println("Voľba 2: Zoradenie psov podľa sfarbenia srsti");
    System.out.println("Voľba 3: Zoradenie psov podľa veku");
    System.out.println("Voľba 4: Návrat do menu");
    System.out.println();
  }

  /* ------------------- */

  @Override
  public void menuAnimalsAll3D() {

    /* Method for listing all animals in start menu */

  if (animals3D.size() != 0) {
    animalListIt3D();
    } else System.out.println("Zoznam psov je prázdny, niet čo zoraďovať!"); /* Zero animals on the 3D list */
  }

  /* ------------------- */

  @Override
  public void delayIt(int i) {

    /* Method for delaying messages */

    try {
      TimeUnit.SECONDS.sleep(i);
    } catch (InterruptedException e) {
      e.printStackTrace(); /* Not sure what else can be here */
    }
  }

}
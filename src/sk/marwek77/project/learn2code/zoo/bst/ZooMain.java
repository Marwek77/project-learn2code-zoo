package sk.marwek77.project.learn2code.zoo.bst; /* bst means breeding station */

class ZooMain {

  /* Class Main for running the program */

    public static void main(String[] args) {

     ZooMenu zooMenu = new ZooMenu();

     zooMenu.menuStart();
    }

}

/*  created by Marek Kamensky
    public source https://Marwek77@bitbucket.org/Marwek77/projectlearn2codezoo.git
    JDK 1.8.0_191, IntelliJ IDEA 2018.3.4
    published on www.learn2code.sk - the task "Naprogramuj Zoo" - "Program the Zoo" on 06.02.2019

    This is my first Java program. My experiences with programming in Java were zero. A long time ago as child I played
    with Atari Basic, from these times I still remember IF THEN GOTO only :) Thanx to Jaro Beno, his course Java for
    beginners and Git for beginners on www.learn2code.sk, great IDE IntelliJ IDEA Community Edition,
    geeksforgeeks.org, beginnersbook.com, stackoverflow.com and other sources I was able to do it!
    Now I will continue with 2nd course from Jaro Beno - Java for advanced and I look forward to other tasks! */
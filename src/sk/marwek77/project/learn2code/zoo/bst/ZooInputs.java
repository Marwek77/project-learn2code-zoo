package sk.marwek77.project.learn2code.zoo.bst;

import java.util.Scanner;
import java.util.InputMismatchException;

class ZooInputs {

  /* This class is for inputs only */

  int readInteger() {

    /* Numbers reading method universal */

    Scanner scan = new Scanner(System.in);
    int number = 0;
    boolean weHaveException = true;

    do {
      try {
        System.out.print("" + "(zadaj svoju voľbu): ");
        number = scan.nextInt();
        weHaveException = false;
      }
      catch (InputMismatchException e) {
        System.out.println("Zadaný reťazec nie je celé číslo.");
        System.out.println("Opakujte zadanie!");
        scan.nextLine();
      }
    } while (weHaveException);

    return number;
  }

  String readString() {

    /* String reading method universal */

    Scanner scan = new Scanner(System.in);
    String string = "";

    try {
        System.out.print("" + "(zadaj reťazec): ");
        string = scan.next();
    }
      catch (Exception e) {
        e.printStackTrace(); /* Not sure what else can be here */
      }
    return string;
  }

}
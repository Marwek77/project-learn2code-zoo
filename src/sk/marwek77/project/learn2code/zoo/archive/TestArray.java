package sk.marwek77.project.learn2code.zoo.archive;

import java.util.ArrayList;
import java.util.List;


public class TestArray extends ZooEntry {

    public static void main(String[] args) {

        List<String> animals = new ArrayList<>();
        int animalsTotal = 5;

        for (int i = 0; i < animalsTotal; ++i)
        {
            animals.add(readString("Zadaj názov zvieraťa číslo "+ (i+1)));
        }

        System.out.println(animals);
    }
}
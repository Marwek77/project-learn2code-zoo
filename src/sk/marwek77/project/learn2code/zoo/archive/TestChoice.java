package sk.marwek77.project.learn2code.zoo.archive;


public class TestChoice extends ZooEntry {

    public static void main(String[] args) {
        System.out.println("\nVitajte v konfigurátore bst! \nKrok 1. zvoľte si prosím veľkosť Vašej bst.");
        System.out.println("\nVoľba 1 - počet zvierat 1 až 5");
        System.out.println("Voľba 2 - počet zvierat 1 až 10");
        System.out.println("Voľba 3 - počet zvierat 1 až 15");

        int counter = 0;
        int animalsNum = 0;
        int animalsTotal = 0;

        do {
            int choice = readInteger("\nVyberte si prosím medzi možnosťami 1 až 3:");

            if ((choice == 1) || (choice == 2) || (choice == 3)) {
                animalsNum = choice;
                counter++;
            } else {
                System.out.println("Môžete si zvoliť len medzi číslami 1, 2 a 3!");
            }
        } while (counter < 1);


        switch (animalsNum)
        {
            case 1: animalsTotal = readInteger("\nVybralis ste si Voľba 1 - počet zvierat 1 až 5, zadajte prosím počet zvierat medzi 1 až 5:"); break;
            case 2: animalsTotal = readInteger("\nVybralis ste si Voľba 2 - počet zvierat 1 až 10, zadajte prosím počet zvierat medzi 1 až 10:"); break;
            case 3: animalsTotal = readInteger("\nVybralis ste si Voľba 2 - počet zvierat 1 až 15, zadajte prosím počet zvierat medzi 1 až 15:"); break;
        }

        System.out.println(animalsTotal);
    }
}

package sk.marwek77.project.learn2code.zoo.archive;

import java.io.BufferedReader;
import java.io.InputStreamReader;


class ZooEntry {

    static String readString(String call)
    {
        String string = "";

        InputStreamReader converter = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(converter);

        try
        {
            System.out.print(call + " (zadaj reťazec): ");
            string = in.readLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return string;
    }

    static int readInteger(String call)
    {
        String string;
        int number = 0;

        InputStreamReader converter = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(converter);

        boolean weHaveException;

        do {
            weHaveException = false;

            try
            {
                System.out.print(call + " (zadaj celé číslo): ");
                string = in.readLine();
                number = Integer.parseInt(string);
            }
            catch (Exception e)
            {
                System.out.println("Zadaný reťazec nie je celé číslo.");
                System.out.println("Opakujte zadanie!");
                weHaveException = true;
            }

        } while (weHaveException);

        return number;
    }

    static double readRealNumber(String call)
    {
        String string;
        double realNumber = 0.0;

        InputStreamReader converter = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(converter);

        boolean weHaveException;

        do {
            weHaveException = false;

            try
            {
                System.out.print(call + " (zadaj reálne číslo): ");
                string = in.readLine();
                realNumber = Double.parseDouble(string);
            }
            catch (Exception e)
            {
                System.out.println("Zadaný reťazec nie je reálne číslo.");
                System.out.println("Opakujte zadanie!");
                weHaveException = true;
            }

        } while (weHaveException);

        return realNumber;
    }

}

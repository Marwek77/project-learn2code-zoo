package sk.marwek77.project.learn2code.zoo.archive;


public class TestEntry extends ZooEntry {

    public static void main(String[] args) {

        String meno = readString("Ahoj ako so voláš?");
        System.out.println("\nAhoj " + meno + ", veľmi ma teší!");

        int a = readInteger("\nZadaj prvé číslo");
        int b = readInteger("Zadaj druhé číslo");
        int c = readInteger("Zadaj tretie číslo");

        int d = a * b * c;
        System.out.println("\nSúčin čísel " + a + " * " + b + " * " + c + " = " + d);

        double x = readRealNumber("\nZadaj prvé číslo");
        double y = readRealNumber("Zadaj druhé číslo");

        double z = x - y;
        System.out.println("\n Rozdiel čísel " + x + " - " + y + " = " + z);
    }
}
